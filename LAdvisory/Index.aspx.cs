﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LAdvisory
{

    public partial class Index : System.Web.UI.Page
    {

        // vajalik hiljem
        bool clear = true;

        // Kood, mis lubab klikkida mitu kuupäeva korraga ja segamini
        // http://jayeshsorathia.blogspot.com.ee/2012/09/beginning-net-multiple-selection-of-date-in-calendar-control.html

        // on/off klikkimine
        // https://stackoverflow.com/questions/26557815/c-sharp-clear-selected-calendar-date-if-it-is-the-same
        // https://asp-net-example.blogspot.com.ee/2011/09/how-to-deselect-calendar-selected-date.html

        // eelmise kuu näitamise eemaldamine
        // https://social.msdn.microsoft.com/Forums/vstudio/en-US/d04f55b2-3276-414e-8581-8892e9f19f6e/aspnet-calendar-disable-previous-month?forum=csharpgeneral

        // kuidas teha terve cell klikitavaks
        // https://stackoverflow.com/questions/619277/net-calendar-making-the-whole-cell-perform-postback-clickable

        

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Page.IsPostBack && Calendar1.SelectedDates.Count == 1)
            {
                Calendar1.SelectedDates.Clear();
            }

            Calendar1.VisibleMonthChanged +=
            new MonthChangedEventHandler(this.Calendar1_VisibleMonthChanged);

            if (Calendar1.VisibleDate.Month == DateTime.Now.Month)
                Calendar1.PrevMonthText = "";

        }

        List<string> Times = new List<string>();
        
        // list(classi omadus), kuhu salvestatakse valitud kuupäevad
        public List<DateTime> MultipleSelectedDates
        {
            get
            {
                // ViewState on dictionary
                // kontrollib, ega MultipleSelectedDates list (ehk kuupäevad, mida kasutaja klikkis aktiivseks) ei ole juba ViewState's olemas
                if (ViewState["MultipleSelectedDates"] == null)
                    // kui ei ole, siis lisatakse kasutaja valitud kuupäevad ViewState dictionary "MultipleSelectedDates" Key'ga
                    ViewState["MultipleSelectedDates"] = new List<DateTime>();
                // tagasi antakse List DateTime objektidest
                return (List<DateTime>)ViewState["MultipleSelectedDates"];
            }
            set
            {
                ViewState["MultipleSelectedDates"] = value;
            }
        }

       

        protected void Calendar1_PreRender(object sender, EventArgs e)
        {
            if (clear == true
            //&& !string.IsNullOrEmpty(Request.Form["__EVENTTARGET"])
            //&& Request["__EVENTTARGET"].Contains(Calendar1.ID)
            //&& char.IsDigit(Request.Form["__EVENTARGUMENT"][0])
            )

                // eemaldab kuupäevad
                Calendar1.SelectedDates.Clear();

            // lisab MultipleSelectedDates objektist kõik kuupäevad SelectedDates objekti
            foreach (DateTime dt in MultipleSelectedDates)
            {
                Calendar1.SelectedDates.Add(dt);
            }
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {

            if (MultipleSelectedDates.Contains(Calendar1.SelectedDate))
            {
                MultipleSelectedDates.Remove(Calendar1.SelectedDate);

            }
            else
            {
                MultipleSelectedDates.Add(Calendar1.SelectedDate);
                clear = false;
            }

            ViewState["MultipleSelectedDates"] = MultipleSelectedDates;


            // TABEL //
            // kui keegi klikib kuupäeval, siis alles generatetakse tabel
            int nRows = 12;
            int nCols = 1;
            int counter = 1;

            

            // tabeli moodustamine
            TableRow r1 = new TableRow();
            TableCell c1 = new TableCell();
            Table1.Rows.Add(r1);
            r1.Cells.Add(c1);
            c1.Text = "<b>Vali kellaajad:</b>";

            for (int i = 0; i < nRows; i++)
            {
                TableRow rw = new TableRow();
                
                for (int j = 0; j < nCols; j++)
                {
                    
                    TableCell cel = new TableCell();

                    //List<string> asjad = new List<string>();

                    // täidame lahtrid nuppudega
                    //Button btn1 = new Button();
                    //btn1.Text = (counter + 7).ToString() + ":00";
                    CheckBox btn1 = new CheckBox();
                    btn1.Text = (counter + 7).ToString() + ":00";

                    cel.Controls.Add(btn1);
                    // nupu välimuse saab määratleda index lehe css klassi alt
                    btn1.CssClass = "Button";
                    
                    rw.Cells.Add(cel);
                    counter++;

                    
                    
                }
                Table1.Rows.Add(rw);   
            }

        }


        //private void Btn1_CheckedChanged(object sender, EventArgs e)
        //{
        //    Times.Add("1");
        //}

        // list kuhu sisse lähevad valitud kellaajad



        // ebaoluline
        // lihtsalt trükib kuupäevad lehele
        protected void btnGetSelectedDate_Click(object sender, EventArgs e)
        {

            foreach (DateTime dt in MultipleSelectedDates)
            {
                lblDate.Text = lblDate.Text + " <br/> " + dt.ToString("dd/MM/yyyy");
            }
        }

        // kuidas lukustada möödunud kuupäevad
        // http://www.codedigest.com/CodeDigest/181-How-to-disable-past-previous-dates-in-Calendar-Control-in-ASP-Net-.aspx

        protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
        {
            // Möödunud kuupäevade stiil
            // https://msdn.microsoft.com/en-us/library/k4t1kwcd.aspx

            Style PastDates = new Style();
            PastDates.BackColor = System.Drawing.Color.White; // seda muuta pärast

            if (e.Day.Date.CompareTo(DateTime.Today) < 0)
            {
                e.Day.IsSelectable = false;
                e.Cell.ApplyStyle(PastDates);
            }
            else
            {
                e.Cell.Attributes.Add("OnClick", e.SelectUrl);
                // pointeri stiili muutmine calendri lahtri sees käeks (hand)
                e.Cell.Style.Add("cursor", "pointer");
            }
            
            
        }


        // seda peab modima, et saada disableda eelmisele kuule minemise
        protected void Calendar1_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {

            DateTime currentDate = DateTime.Now;
            DateTime dateOfMonthToDisable = currentDate.AddMonths(-1);

            // kui uus kuupäev on kuhu tahan minna on eelmine kuu
            if (e.NewDate.Month == dateOfMonthToDisable.Month)
            {
                // siis näita eelmise kuu asemel seda kuud, kust ma hakkasin klikkima
                Calendar1.VisibleDate = e.PreviousDate;
            }

            // kui tagasi tulla praeguse kuu juurde, siis kontrollib
            // kas hetkel ees olev kuu on praegune kuu
            if(Calendar1.VisibleDate.Month == DateTime.Now.Month)
                // kui true, siis eemaldab teksti
                Calendar1.PrevMonthText = "";
            else
                // vastasel juhul lisab < märgi tagasi
                Calendar1.PrevMonthText = "<";
        }

        protected void nupp1_Click(object sender, EventArgs e)
        {
            foreach (var item in Times)
            {
                Label2.Text = item;
            }
        }
    }
}