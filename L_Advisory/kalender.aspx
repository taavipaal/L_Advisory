﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="kalender.aspx.cs" Inherits="LAdvisory.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
      <%--<link href="CalendarStyle.css" rel="stylesheet" type="text/css" />--%>
<head runat="server">
    <title></title>
    <style>
        #Calendar {width:66.666%;
                   float:left;
        }
        #Table1 {width:10%;
                    text-align:center;
                    /*float:left;*/
                    margin:0;
                    padding:0;
        }
        #Table1:focus {
            background-color: black;
        }
        .Button {
            line-height:1.6;
            width: 200px;
            margin:4px;
            background-color:#EFEFEF;
            border-radius:4px;
            border:1px solid #D0D0D0;
            overflow:auto;
            float:left;
        }
        .Button:hover {
            background-color: forestgreen;
        }
        .Button:checked {
            background-color: red;
        }
        .Button label {
             float:left;
             width:4.0em;
        }
        .Button label span {
    text-align:center;
    padding:3px 0px;
    display:block;
}

.Button label input {
    position:absolute;
    top:-20px;
}

.Button input:checked + span {
    background-color:#911;
    color:#fff;
}
        /*.DayClass {cursor:pointer;}*/
        /*.Calendar1 A:visited {text-decoration: none;}
        .Calendar1 A:hover {text-decoration: none;}
        .Calendar1 A:active {text-decoration: none;}*/
    </style>
</head>
<body>
  <div id="Container">
    <form id="form1" runat="server">
        <div id="Calendar">
            <asp:Calendar ID="Calendar1" 
                runat="server"
                BackColor="White" 
                BorderColor="Black"
                Font-Names="Verdana" 
                Font-Size="9pt" 
                ForeColor="Black" 
                Width="400"
                Height="400"
                OnPreRender="Calendar1_PreRender"
                OnSelectionChanged="Calendar1_SelectionChanged" 
                BorderStyle="Solid"
                CellSpacing="1" 
                OnDayRender="Calendar1_DayRender" 
                OnVisibleMonthChanged="Calendar1_VisibleMonthChanged" >
                <DayHeaderStyle Font-Bold="True" 
                                Font-Size="8pt" 
                                ForeColor="#333333"
                                Height="8pt" />
                <DayStyle BackColor="#CCCCCC"/>
                <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                <TitleStyle BackColor="#333399" Font-Bold="True"
                            Font-Size="12pt" ForeColor="White" BorderStyle="Solid" Height="12pt" />
            </asp:Calendar>
            

            <br />
            <br />
            <asp:Button runat="server" 
                        ID="btnGetSelectedDate" 
                        Text="Get Selected Date"
                        OnClick="btnGetSelectedDate_Click" />
            &nbsp;&nbsp;&nbsp;<br />
            <b>Selected Dates : </b>
            <asp:Label runat="server" ID="lblDate"></asp:Label>   
        </div>
        
    </form>
  </div>
</body>
</html>

<%--<div id="TimeTable">
            
            <asp:Table ID="Table1" runat="server" >

            </asp:Table>

            <asp:Button ID="nupp1" Text="Nuputa" runat="server" OnClick="nupp1_Click" />
            <b>Kontroll, kas ajal lähevad kuhu vaja</b>
            <asp:Label runat="server" ID="Label2"></asp:Label>

        </div>--%>