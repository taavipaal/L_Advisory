﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(L_Advisory.Startup))]
namespace L_Advisory
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
