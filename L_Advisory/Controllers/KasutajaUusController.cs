﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using L_Advisory;
using System.Text;
using System.IO;

namespace L_Advisory
{
    public partial class KasutajaUus
    {
        static L_AdvisoryEntities db = new L_AdvisoryEntities();

        // NEED TEGIME ISE
        public string FullName => Eesnimi + " " + Perenimi;
        public string HinnaVahemik
        {
           get
            {
                if(MinTunniHind != null)
                {
                    return MinTunniHind.ToString().Trim('0').Trim(',') + " - " + MaxTunniHind.ToString().Trim('0').Trim(',') + "€";
                }
                else
                {
                    return "-";
                }
            }
        }

        // funktsioon, mis leiab emaili järgi kasutaja
        public static KasutajaUus ByEmail(string email)
        {
            return db.KasutajaUus.Where(x => x.Email == email).Take(1).SingleOrDefault();
        }

        // leiab id järgi kasutaja
        public static KasutajaUus ById(int id)
        {
            return db.KasutajaUus.Where(x => x.Id == id).Take(1).SingleOrDefault();
        }

        // dictionary keskmiste hinnetega (hiljem peab id järgi ise otsima õiged read välja)
        public Dictionary<int?, double?> KeskmisedHinded
        {
            get
            {   
                var keskmine = db.Tagasiside
                    .Where(x => x.Hinne > 0)
                    .ToLookup(x => x.KlientKey)
                    .Select(x => new { KlientKey = x.Key, Keskmine = x.Average(y => y.Hinne) })
                    .ToDictionary(x => x.KlientKey, x => x.Keskmine);

                return keskmine;
            }
        }
    }
}

namespace L_Advisory.Controllers
{
    public class KasutajaUusController : Controller
    {
        private L_AdvisoryEntities db = new L_AdvisoryEntities();

        public ActionResult Photo(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            KasutajaUus e = db.KasutajaUus.Find(id.Value);
            if (e == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (e.Pilt?.Length == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var picture = e.Pilt.ToArray();
            //if (picture[0] != 255) picture = picture.Skip(78).ToArray();
            return File(picture, "image/jpg");
        }



        // GET: KasutajaUus
        public ActionResult Index()
        {
            //if (Request.IsAuthenticated)
            //{

            KasutajaUus k = KasutajaUus.ByEmail(User.Identity.Name);

                var kasutajaUus = db.KasutajaUus.Include(m => m.Kalender);

                return View(kasutajaUus.ToList());
            //}
            //return HttpNotFound();
        }

        public ActionResult ActionThatActionThatRetrieveAndAspx()
        {
            return View("kalender");
        }


        // GET: KasutajaUus/Details/5
        public ActionResult Details(int? id)
        {
            //if(Request.IsAuthenticated)
            //{
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                KasutajaUus kasutajaUus = db.KasutajaUus.Find(id);

                if (kasutajaUus == null)
                {
                    return HttpNotFound();
                }
                return View(kasutajaUus);
            //}
            //return HttpNotFound();
        }

        // GET: KasutajaUus/Create
        public ActionResult Create()
        {
            ViewBag.Id = new SelectList(db.Kalender, "Key", "Key");
            return View();
        }

        // POST: KasutajaUus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Eesnimi,Perenimi,Email,Soov,Telefon,Position,Company,Asukoht,Valdkond,Tutvustus,MinTunniHind,MaxTunniHind,OnSpetsialist,Pilt")] KasutajaUus kasutajaUus, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {

                db.Entry(kasutajaUus).State = EntityState.Modified;

                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        kasutajaUus.Pilt = buff;
                    }
                }

                db.KasutajaUus.Add(kasutajaUus);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id = new SelectList(db.Kalender, "Key", "Key", kasutajaUus.Id);
            return View(kasutajaUus);
        }

        // GET: KasutajaUus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaUus kasutajaUus = db.KasutajaUus.Find(id);

            if (kasutajaUus == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id = new SelectList(db.Kalender, "Key", "Key", kasutajaUus.Id);
            return View(kasutajaUus);
        }

        // POST: KasutajaUus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Eesnimi,Perenimi,Email,Soov,Telefon,Position,Company,Asukoht,Valdkond,Tutvustus,MinTunniHind,MaxTunniHind,OnSpetsialist,Pilt")] KasutajaUus kasutajaUus, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kasutajaUus).State = EntityState.Modified;


                db.Entry(kasutajaUus).Property(x => x.Pilt).IsModified = false;
                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        kasutajaUus.Pilt = buff;
                    }
                }

                db.SaveChanges();
                return RedirectToAction($"Details/{kasutajaUus.Id}", "KasutajaUus");
            }
            ViewBag.Id = new SelectList(db.Kalender, "Key", "Key", kasutajaUus.Id);
            return View(kasutajaUus);
        }

        // GET: KasutajaUus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaUus kasutajaUus = db.KasutajaUus.Find(id);
            if (kasutajaUus == null)
            {
                return HttpNotFound();
            }
            return View(kasutajaUus);
        }

        // POST: KasutajaUus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KasutajaUus kasutajaUus = db.KasutajaUus.Find(id);
            db.KasutajaUus.Remove(kasutajaUus);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
