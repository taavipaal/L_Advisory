﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using L_Advisory;
using System.Web.UI.WebControls;

namespace L_Advisory
{
    public partial class Tagasiside
    {
        static L_AdvisoryEntities db = new L_AdvisoryEntities();

        public SelectList TagasisideHinded { get; set; }

        public static Tagasiside ById(int id)
        {
            return db.Tagasiside.Where(x => x.KlientKey == id).Take(1).SingleOrDefault();
        }

        public int UuedTagasisided
        {
            set
            {

            }
        }
        
    }
}

namespace L_Advisory.Controllers
{
    public class TagasisidesController : Controller
    {
        private L_AdvisoryEntities db = new L_AdvisoryEntities();

        

        // GET: Tagasisides
        public ActionResult Index()
        {
            var tagasiside = db.Tagasiside.Include(t => t.KasutajaUus).Include(t => t.KasutajaUus1);
            return View(tagasiside.ToList());
        }

        // GET: Tagasisides/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tagasiside tagasiside = db.Tagasiside.Find(id);
            if (tagasiside == null)
            {
                return HttpNotFound();
            }
            return View(tagasiside);
        }

        

        // GET: Tagasisides/Create
        public ActionResult Create(int? id)
        {
            List<SelectListItem> hinded = new List<SelectListItem>()
            {
            new SelectListItem() { Text = "1",Value = "1" },
            new SelectListItem() { Text = "2",Value = "2" },
            new SelectListItem() { Text = "3",Value = "3" },
            new SelectListItem() { Text = "4",Value = "4" },
            new SelectListItem() { Text = "5",Value = "5" },
            };

            int klientKey = id.Value / 10000;
            int spetsialistKey = id.Value % 10000;

            Tagasiside tagasiside = new Tagasiside { KlientKey = klientKey, SpetsialistKey = spetsialistKey,
                                                     TagasisideHinded = new SelectList(hinded, "Value", "Text") };

            ViewBag.KlientKey = new SelectList(db.KasutajaUus, "Id", "FullName", tagasiside.KlientKey);
            ViewBag.SpetsialistKey = new SelectList(db.KasutajaUus, "Id", "FullName", tagasiside.SpetsialistKey);

            
            return View(tagasiside);
        }

        // POST: Tagasisides/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "nr,KlientKey,SpetsialistKey,Hinne,Tagasiside1")] Tagasiside tagasiside)
        {
            ViewBag.KlientKey = new SelectList(db.KasutajaUus, "Id", "Eesnimi", tagasiside.KlientKey);
            ViewBag.SpetsialistKey = new SelectList(db.KasutajaUus, "Id", "Eesnimi", tagasiside.SpetsialistKey);

            if (ModelState.IsValid)
            {
                db.Tagasiside.Add(tagasiside);
                db.SaveChanges();
                // viib teisse kohta, kui alguses kirjas oli
                // saab panna custom url-e
                return RedirectToAction($"Details/{tagasiside.KlientKey}", "KasutajaUus");
            }
            
            return View(tagasiside);

        }

        // GET: Tagasisides/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tagasiside tagasiside = db.Tagasiside.Find(id);
            if (tagasiside == null)
            {
                return HttpNotFound();
            }
            ViewBag.KlientKey = new SelectList(db.KasutajaUus, "Id", "Eesnimi", tagasiside.KlientKey);
            ViewBag.SpetsialistKey = new SelectList(db.KasutajaUus, "Id", "Eesnimi", tagasiside.SpetsialistKey);
            return View(tagasiside);
        }

        // POST: Tagasisides/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "nr,KlientKey,SpetsialistKey,Hinne,Tagasiside1")] Tagasiside tagasiside)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tagasiside).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KlientKey = new SelectList(db.KasutajaUus, "Id", "Eesnimi", tagasiside.KlientKey);
            ViewBag.SpetsialistKey = new SelectList(db.KasutajaUus, "Id", "Eesnimi", tagasiside.SpetsialistKey);
            return View(tagasiside);
        }

        // GET: Tagasisides/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tagasiside tagasiside = db.Tagasiside.Find(id);
            if (tagasiside == null)
            {
                return HttpNotFound();
            }
            return View(tagasiside);
        }

        // POST: Tagasisides/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tagasiside tagasiside = db.Tagasiside.Find(id);
            db.Tagasiside.Remove(tagasiside);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
