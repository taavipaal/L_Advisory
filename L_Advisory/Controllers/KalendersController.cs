﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using L_Advisory;
using System.Text;

namespace L_Advisory
{
    public partial class Kalender
    {
        // tänane kuupäev
        public int TänaneKp { get { return DateTime.Now.Day; } }

        // päevade arv antud kuus
        public int PäevadeArvKuus { get => DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month); }

    }
}

namespace L_Advisory.Controllers
{

    public class KalendersController : Controller
    {
        public ActionResult BroneeringComplete()
        {
            return View();
        }

        public ActionResult Kalender(int? id)
        {
            DateTime Täna = DateTime.Now.Date.AddDays(id ?? 0);
            DateTime AlgKp = Täna.AddDays(1 - Täna.Day);
            DateTime AlgRida = AlgKp.AddDays(0 - (int)(AlgKp.DayOfWeek));
            ViewBag.Täna = Täna;
            ViewBag.AlgKp = AlgKp;
            ViewBag.AlgRida = AlgRida;
            ViewBag.KalenderId = id ?? 0;

            return View(db.Kalender.ToLookup(x => x.Kuupäev).ToDictionary(x => x.Key, x => x.ToList()));
        }

        private L_AdvisoryEntities db = new L_AdvisoryEntities();

        // GET: Kalenders
        public ActionResult Index()
        {
            var kalender = db.Kalender.Include(k => k.KasutajaUus);
            return View(kalender.ToList());
        }

        // GET: Kalenders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kalender kalender = db.Kalender.Find(id);
            if (kalender == null)
            {
                return HttpNotFound();
            }
            return View(kalender);
        }

        // GET: Kalenders/Create
        public ActionResult Create(int? id)
        {
            int MinaKey = id.Value % 10000;
            int spetsialistKey = id.Value / 10000;
            int Staatus = 1;

            Kalender kalender = new Kalender
            {
                Key = spetsialistKey,
                Kuupäev = DateTime.Now.Date
            };

            ViewBag.St1 = Staatus;
            ViewBag.MinaKey = MinaKey;
            ViewBag.Key = new SelectList(db.KasutajaUus, "Id", "FullName", kalender.Key);
            return View(kalender);
        }

        // POST: Kalenders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]        
        public ActionResult Create([Bind(Include = "nr,Key,Kuupäev,Tund,TunniHInd,Staatus")] Kalender kalender)
        {
            if (ModelState.IsValid)
            {
                db.Kalender.Add(kalender);
                db.SaveChanges(); // siin tekib kalendrile key (nr)
                return RedirectToAction($"Details/{kalender.Key}", "KasutajaUus");
            }

            ViewBag.Key = new SelectList(db.KasutajaUus, "Id", "Eesnimi", kalender.Key);
            return View(kalender);
        }

        // GET: Kalenders/Edit/5
        public ActionResult Edit(int? id)
        {

            

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Kalender kalender = db.Kalender.Find(id);

            if (kalender == null)
            {
                return HttpNotFound();
            }

            ViewBag.Key = new SelectList(db.KasutajaUus, "Id", "Eesnimi", kalender.Key);
            return View(kalender);
        }

        // POST: Kalenders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "nr,Key,Kuupäev,Tund,TunniHInd,Staatus")] Kalender kalender)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kalender).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction($"Details/{kalender.Key}", "KasutajaUus");
                //return RedirectToAction("Index");
            }

            //ViewBag.nr = kalender.nr;
            ViewBag.Key = new SelectList(db.KasutajaUus, "Id", "Eesnimi", kalender.Key);
            return View(kalender);
        }

        // GET: Kalenders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kalender kalender = db.Kalender.Find(id);
            if (kalender == null)
            {
                return HttpNotFound();
            }
            return View(kalender);
        }

        // POST: Kalenders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kalender kalender = db.Kalender.Find(id);
            db.Kalender.Remove(kalender);
            db.SaveChanges();
            return RedirectToAction($"Details/{kalender.Key}", "KasutajaUus");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
